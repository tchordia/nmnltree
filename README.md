# Repo for NMNL build logic

## Install

First pull repo into folder.
`conda install --file package-list.txt`

## Run main test file (will test on default trees)
```
python -m sprout.run
```

## Summarized Directory Structure

Most important files: Oracle.py, nmnl_impl.py, trees.py, run.py

```
.
├── README.md
├── package-list.txt
├── res
├── sprout
│   ├── __init__.py
│   ├── analysis
│   │   ├── __init__.py
│   │   ├── analysis.py
│   │   └── run_test.py
│   ├── deprecated_files
│   ├── nmnl (CORE nmnl files)
│   │   ├── nmnl_impl.py
│   │   ├── node.py
│   │   └── oracle.py
│   ├── test
│   │   ├── generate_synthetic_data.py
│   │   ├── nick_trees.py
│   │   └── trees.py
│   ├── main.py (main run script)
│   ├── run.py
│   ├── consts.py
.   └── utils.py

```