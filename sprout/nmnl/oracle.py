# Given the data, construct an oracle
import logging as lg
import math

import networkx as nx
import numpy as np
from scipy import stats
from statsmodels.stats.proportion import proportions_ztest as ztest

olog = lg.getLogger(__name__)

class Oracle:
    """
        Class that implements both the Sibling Oracle and the UncleOracle
    """

    def __init__(self, params):

        data, self.alpha, self.uncle_alpha, self.max_cut_value, _ = params

        self.data = data.copy()
        self.orig_data = data.copy()

        self.cols = self.data.shape[1]
        self.cur_merged = 0
        self.sets = {i: {i} for i in range(self.cols)}
        self.idDict = {}
        self.idCounter = -1

        olog.debug(("Data shape", data.shape))

    def getTree_uncle(self):

        """
        Builds the uncle tree with the uncle algorithm. The single point of entry for this algorithm.

        :return: Returns a merge history. This is a list of tuples, of the form
        [(-1, [1, 2, 3]), (-2, [-1, 4])]

        From this list of tuples, we can reconstruct the original tree.

        :rtype:
        """

        mergeHistory = []

        X = self.getMatVect_chi2()

        olog.debug(('Distance matrix', X))
        olog.debug(('Sets', self.sets))

        setList, singletonList = self.computeSets(X)

        uncleGraph = self.initUncleGraph(setList)

        sortedSetList = self.topological_sort_reverse(uncleGraph)

        olog.debug(('set list', setList))
        olog.debug(('singleton list', singletonList))
        olog.debug(('uncle graph', uncleGraph.nodes(), uncleGraph.edges()))
        olog.debug(('SortedSetList', sortedSetList))

        while len(sortedSetList) > 0:
            olog.debug(('\n'))
            olog.debug(('********** iterate **********'))
            olog.debug(('Set List', setList))
            olog.debug(('Singleton List', singletonList))
            olog.debug(('Sorted Set List Initial:', sortedSetList))
            olog.debug(('uncle graph', uncleGraph.nodes(), uncleGraph.edges()))
            olog.debug((''))

            nextSetNumber = sortedSetList[0]
            nextSet = setList[nextSetNumber]
            combined_item = next(iter(nextSet))
            combined_list = list(nextSet)
            self.merge(combined_item, combined_list)

            setList[nextSetNumber] = set()

            uncleGraph.remove_node(nextSetNumber)

            mergeHistory.append((self.idCounter, self.getIdList(combined_list)))

            self.idDict[combined_item] = self.idCounter
            self.idCounter -= 1

            del sortedSetList[0]

            olog.debug(('Combined item: ', combined_item))
            olog.debug(('sortedSetList', sortedSetList, 'Set list', setList))

            if len(sortedSetList) == 0:
                if len(singletonList) > 0:
                    singletonList.append(set([combined_item]))
                    to_merge = [self.idDict[x] for s in singletonList for x in s]
                    mergeHistory.append((self.idCounter, to_merge))
                break

            # try to merge into existing equivalence sets
            flag = False
            for j in range(0, len(sortedSetList)):

                setToCheck = setList[sortedSetList[j]]
                olog.debug(('Set to check', setToCheck))
                for set_item in setToCheck:
                    olog.debug(('Checking equivelence', set_item, combined_item))

                    if self.areEqual(set_item, combined_item):
                        olog.debug(('Set to check', setToCheck))

                        flag = True
                        break

                if flag:
                    olog.debug(('Set to check:', setToCheck, ' Combined Item', combined_item))
                    setToCheck.add(combined_item)
                    olog.debug(('Set added to: ', setToCheck))
                    break

            # wasn't merged into any equivalence set, try to merge with a singleton
            if not flag:

                olog.debug(('\nComparing to singleton sets'))
                olog.debug(('SingletonList', singletonList))
                new_set = None
                for singletonSet in singletonList:
                    singleton = next(iter(singletonSet))

                    olog.debug(('Check if equivalent', singleton, combined_item))
                    if self.areEqual(singleton, combined_item):
                        new_set = {combined_item, singleton}
                        olog.debug(('Equal, new set', new_set))
                        break

                if new_set is None:
                    # We have to create a new singleton

                    olog.debug(('New singleton', {combined_item}))
                    singletonList.append({combined_item})

                else:

                    # We have found a new home

                    olog.debug(('Found a home', new_set))
                    singletonList.remove({singleton})
                    setList.append(new_set)
                    new_set_idx = len(setList) - 1
                    uncleGraph.add_node(new_set_idx)

                    for uncle_id in uncleGraph:

                        olog.debug(('Checking is uncle', self.getIdList(setList[uncle_id]), self.getIdList(new_set)))
                        new_uncle = self.isUncle(setList[uncle_id], new_set)
                        if new_uncle == new_set:
                            self.add_dag_edge(uncleGraph, new_set_idx, uncle_id)
                        elif new_uncle == setList[uncle_id]:
                            self.add_dag_edge(uncleGraph, uncle_id, new_set_idx)

                    olog.debug(("New final uncle graph", uncleGraph.nodes(), uncleGraph.edges()))
                    sortedSetList = self.topological_sort_reverse(uncleGraph)
                    olog.debug(("new sorted set list", sortedSetList))

        olog.debug(('Exiting'))
        olog.debug(('Set List', setList))
        olog.debug(('Singleton List', singletonList))
        olog.debug(('Sorted Set List Initial:', sortedSetList))
        olog.debug(('uncle graph', uncleGraph))
        return mergeHistory

    def isUncle(self, equivSet1, equivSet2):

        """
        Uncle Oracle implementation. Determines whether one equivalence set is the uncle of another.
        Implementation is as described in the paper.

        :param equivSet1:
        :type equivSet1:
        :param equivSet2:
        :type equivSet2:
        :return:
        :rtype:
        """

        olog.debug(('Inputs', equivSet1, equivSet2))

        # basic checks
        if (len(equivSet1) < 2 or len(equivSet2) < 2 or equivSet1 == equivSet2):
            return None

        # grab first 2 items
        iter1 = iter(equivSet1)
        iter2 = iter(equivSet2)

        i = next(iter1)
        j = next(iter2)

        k1 = next(iter1)
        k2 = next(iter2)

        olog.debug(("Vals, i, j, k1, k2", i, j, k1, k2))

        p_i1, p_j1, p_k1, count_1, p_ijk1 = self.p_ijk(i, j, k1)
        p_i2, p_j2, p_k2, count_2, p_ijk2 = self.p_ijk(i, j, k2)

        olog.debug(("Count1, count2:", count_1, count_2))

        count_max = (count_1 + count_2) / 2

        # check if we have a situation where we can compute p(i, j) exactly
        p_ij_tup = self.compute_pij_exact(i, j)
        if p_ij_tup is not None:
            wins_i = p_ij_tup[0]
            wins_all = p_ij_tup[1]
            _, pval_pair_1 = ztest([wins_i, p_ijk1 * count_1], [wins_all, count_1], alternative='larger')
            _, pval_pair_2 = ztest([wins_i, p_ijk2 * count_2], [wins_all, count_2], alternative='smaller')
            if pval_pair_2 < self.uncle_alpha or pval_pair_1 < self.uncle_alpha:
                olog.debug(("Resolving with pair comparison"))
                olog.debug(("wins_i, wins_all", wins_i, wins_all))
                olog.debug(("p_i1, p_i2, count", wins_i / wins_all, p_ijk1, p_ijk2, count_1, count_2))
                olog.debug(("Pval1, pval2", pval_pair_1, pval_pair_2))
                uncle_ret = equivSet2 if pval_pair_1 < pval_pair_2 else equivSet1
                olog.debug(("UNcle", uncle_ret))
                return uncle_ret

        uncle_test_i = np.asarray([
            [p_i2, (p_i1 / (1 - p_k1))],
            [p_j2, (p_j1) / (1 - p_k1) - p_k2]
        ]) * count_max

        uncle_test_j = np.asarray([
            [p_i2, (p_i1 + p_k1) * (1 - p_k2)],
            [p_j2, p_j1 * (1 - p_k2)]
        ]) * count_max

        peer_test = np.asarray([
            [p_i2, p_i1 + p_k1],
            [p_j2, p_j1 - p_k2]
        ]) * count_max

        olog.debug((uncle_test_i))
        olog.debug((uncle_test_j))
        olog.debug((peer_test))

        valid = np.asarray([np.all(uncle_test_i >= 5), np.all(uncle_test_j >= 5), np.all(peer_test >= 5)])
        all_valid = np.all(valid)

        if all_valid:
            chi21, pval_uncle_i, _, _ = stats.chi2_contingency(uncle_test_i)
            chi21, pval_uncle_j, _, _ = stats.chi2_contingency(uncle_test_j)
            chi21, pval_peer, _, _ = stats.chi2_contingency(peer_test)

            olog.debug(("Tables"))
            olog.debug(("p-values", pval_peer, pval_uncle_i, pval_uncle_j))

            unclePresent = pval_peer < self.uncle_alpha

            ret_uncle = None if not unclePresent else (equivSet1 if pval_uncle_i > pval_uncle_j else equivSet2)
            olog.debug(("Uncle is: ", ret_uncle))
            return ret_uncle

        else:
            return None

    def getMatVect_chi2(self):
        """
        Compute the original chi squared distance matrix
        :return:
        :rtype:
        """
        mat = np.asarray([[self.compute_chi_sq(i, j) for j in range(self.cols)] for i in range(self.cols)])

        return mat

    def update(self, x_id, y_id):
        data = self.data
        data[:, y_id] = np.maximum(data[:, x_id], data[:, y_id])
        data[:, x_id] = np.nan

    def compute_chi_sq_full(self, i, j):
        return 1 - np.sqrt(self.compute_chi_sq(i, j) * self.compute_chi_sq(j, i))

    def merge(self, x, toMerge):
        val = np.amax(self.data[:, toMerge], axis=1)
        self.data[:, toMerge] = np.nan
        self.data[:, x] = val

    def areEqual(self, a, b):

        val = self.compute_chi_sq(a, b, should_print=True)
        olog.debug(('Distance', val))
        return val < 1 - self.alpha

    def getIdValue(self, a):
        return self.idDict[a] if a in self.idDict else a

    def getIdList(self, l):
        return [self.getIdValue(x) for x in l]

    def addToSetListRecur(self, singletonList, setList, subgraphs):

        for x in subgraphs:
            olog.debug(('Len x', len(x)))
            if len(x) == 1:
                singletonList.append(set(x.nodes()))
            elif len(x) < 4:
                setList.append(set(x.nodes()))
            else:
                cut_value, partition = nx.stoer_wagner(x)
                olog.debug(('Min cut:', cut_value, partition))
                if cut_value < self.max_cut_value:
                    self.addToSetListRecur(singletonList, setList, [x.subgraph(partition[0]), x.subgraph(partition[1])])
                else:
                    setList.append(set(x.nodes()))

    def computeSets(self, distX):
        G = nx.Graph()

        G.add_nodes_from(range(self.cols))

        for i in range(0, self.cols - 1):
            for j in range(i + 1, self.cols):
                if 1 - distX[i, j] > self.alpha:
                    G.add_weighted_edges_from([(i, j, 1 - distX[i, j])])

        nx.draw(G)
        subgraphs = nx.connected_component_subgraphs(G)

        setList = []
        singletonList = []

        self.addToSetListRecur(singletonList, setList, subgraphs)

        return setList, singletonList

    def compute_pij_exact(self, i, j):
        data = self.data
        if np.isnan(data[0, i]) or np.isnan(data[1, j]):
            olog.debug(('Nan ', i, j))

        row_2_pts = np.sum(data >= 0, 1) == 2
        pres = np.prod(data[:, [i, j]] >= 0, 1) * row_2_pts
        if sum(pres) < 6:
            return None

        wins_i = np.sum(pres * (data[:, i] > 0))
        wins_j = np.sum(pres * (data[:, j] > 0))

        return wins_i, wins_i + wins_j

    def compute_pij(self, i, j):

        data = self.data
        if np.isnan(data[0, i]) or np.isnan(data[1, j]):
            olog.debug('Nan: %s, %s', i, j)

        pres = np.prod(data[:, [i, j]] >= 0, 1)
        wins_i = np.sum(pres * (data[:, i] > 0)) + 1.0
        wins_j = np.sum(pres * (data[:, j] > 0)) + 1.0
        # olog.debug((wins_i, wins_j, " p" + str(i) + str(j), wins_i/wins_j))
        return wins_i / (wins_j + wins_i)

    def compute_list(self, i, j, k, p_ij):
        data = self.data

        if np.isnan(data[0, i]) or np.isnan(data[1, j]) or np.isnan(data[0, k]):
            olog.debug('Nan ijk: %s, %s, %s', i, j, k)

        pres = np.prod(data[:, [i, j, k]] >= 0, 1)

        wins_i = np.sum(pres * (data[:, i] > 0)) + 1.0
        wins_j = np.sum(pres * (data[:, j] > 0)) + 1.0
        expected = (wins_i + wins_j) * p_ij

        return [wins_i, expected], [wins_j, wins_i + wins_j - expected]

    def chi_sq_test(self, arr):
        return stats.chisquare(f_obs=arr[:, 0], f_exp=arr[:, 1])

    def compute_chi_sq(self, i, j, should_print=False):
        # if i == 15 and j == 14:
        #     print('check')

        if i == j:
            return 0

        data = self.data
        pij = self.compute_pij(i, j)

        pair = [self.compute_list(i, j, k, pij) for k in range(self.cols) if
                k != i and k != j and not np.isnan(data[1, k])]

        t1 = [x[0] for x in pair]
        t2 = [x[1] for x in pair]

        l1 = np.asarray(t1)
        l2 = np.asarray(t2)

        if should_print:
            olog.debug(("Chi^2 arrays", l1, l2))

        # Handle specially the case where you have small tables
        if len(t1) < 2 or len(t2) < 2:
            chi, pval = self.chi_sq_test(np.asarray(t1 + t2))
            distance = round(1 - pval, 3)
        else:
            chi21, pval1 = self.chi_sq_test(l1)
            chi22, pval2 = self.chi_sq_test(l2)

            distance = round(1 - (pval1 * pval2) ** .5, 3)

        return distance

    def p_ijk(self, i, j, k):
        data = self.data
        olog.debug('i, j, k: %s, %s, %s', i, j, k)

        pres = np.prod(data[:, [i, j, k]] >= 0, 1)

        count = np.sum(pres) + 3
        wins_i = np.sum(pres * (data[:, i] > 0)) + 1.0
        wins_j = np.sum(pres * (data[:, j] > 0)) + 1.0
        wins_k = np.sum(pres * (data[:, k] > 0)) + 1.0
        return wins_i / count, wins_j / count, wins_k / count, count, wins_i / (wins_j + wins_i)

    def topological_sort_reverse(self, graph):
        return list(reversed(list(nx.topological_sort(graph))))

    def add_dag_edge(self, G, u, v):
        G.add_edge(u, v)
        if not nx.is_directed_acyclic_graph(G):
            G.remove_edge(u, v)

        return G

    def initUncleGraph(self, setList):
        uncleG = nx.DiGraph()
        uncleG.add_nodes_from(range(len(setList)))

        for i in range(len(setList) - 1):
            for j in range(i + 1, len(setList)):
                uncle = self.isUncle(setList[i], setList[j])

                if uncle == setList[i]:
                    self.add_dag_edge(uncleG, i, j)
                elif uncle == setList[j]:
                    self.add_dag_edge(uncleG, j, i)

        return uncleG

    def getSetIdsAndList(self):
        equivSetIds = set()
        equivSetList = []

        singletonsList = []
        for x in self.sets.values():
            if not id(x) in equivSetIds:
                if len(x) == 1:
                    singletonsList.append(x)
                else:
                    equivSetList.append(x)
                equivSetIds.add(id(x))

        return equivSetList, singletonsList

    @DeprecationWarning
    def getMatVect(self):
        """
        No longer supported.
        :return:
        :rtype:
        """
        mat = np.asarray([[self.mu_ij_vect(i, j) for j in range(self.cols)] for i in range(self.cols)])
        return mat

    @DeprecationWarning
    def mu_ij_vect(self, i, j):
        data = self.data
        cols = data.shape[1]
        p_ijji = self.compute_pijji(i, j)
        # olog.debug(('pijji', p_ijji))
        valk = lambda k: abs(math.log(p_ijji / self.compute_pijkjik(i, j, k)))
        return round(np.sum(np.asarray([valk(k) for k in range(cols) if k != i and k != j])), 3)

    @DeprecationWarning
    def compute_pijkjik(self, i, j, k):
        data = self.data
        pres = np.prod(data[:, [i, j, k]] >= 0, 1)
        wins_i = np.sum(pres * (data[:, i] > 0)) + 1.0
        wins_j = np.sum(pres * (data[:, j] > 0)) + 1.0
        # olog.debug(("p" + str(i) + str(j) + str(k), wins_i, wins_j))
        return wins_i / wins_j

    @DeprecationWarning
    def compute_pijji(self, i, j):
        data = self.data
        pres = np.prod(data[:, [i, j]] >= 0, 1)
        wins_i = np.sum(pres * (data[:, i] > 0)) + 1.0
        wins_j = np.sum(pres * (data[:, j] > 0)) + 1.0
        return wins_i / wins_j
