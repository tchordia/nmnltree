import math


class Node:
    """
    Class represents a single node in the NMNL tree. Autoincrements node counts.
    """

    nextLeafId = 1
    nextInnerId = -1

    @staticmethod
    def resetCount():
        Node.nextLeafId = 1
        Node.nextInnerId = -1

    def __init__(self, children=None, weight=0, isLeaf=False, parent=None):
        self.children = [] if children is None else children
        self.weight = weight
        self.gradient = 0
        self.parent = parent
        self.isLeaf = isLeaf
        self.numNodes = 0

        self.nf = 0
        self.gradsum = 0

        if isLeaf:
            self.id = Node.nextLeafId
            Node.nextLeafId += 1
        else:
            self.id = Node.nextInnerId
            Node.nextInnerId -= 1

        self.isConstrained = False

    def setChildren(self, children):
        self.children = children

    def addChild(self, child):
        self.children.append(child)

    def siblings(self):
        return [] if self.parent == None else self.parent.children

    def len(self):
        return len(self.children)

    def constrain(self):
        self.isConstrained = True;

    def numChildren(self):
        return len(self.children)

    def incrnf(self, by=1):
        self.nf += by

    def exp_weight(self):
        return math.exp(self.weight)

    def updategradsum(self, val):
        self.gradsum += val

    def grad(self):
        return -self.nf + (math.exp(self.weight) * self.gradsum)

    def reset(self):
        self.nf = 0
        self.gradsum = 0

    def __getitem__(self, key):
        return self.children[key]

    def __contains__(self, item):
        return item in self.children

    def __iadd__(self, other):
        self.addChild(other)

    def __iter__(self):
        return iter(self.children)

    # def __str__(self):
    #     return "(id: " + str(self.id) + ", weight: " + str(self.weight) + ", children: " + str([str(x) for x in self.children]) + ")"

    def __str__(self, level=0):
        ret = "\t"*level+str(self.id)+"\n"

        for child in self.children:
            ret += child.__str__(level+1)

        return ret



