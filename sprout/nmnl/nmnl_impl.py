import logging as lg
import random
from math import exp
from math import log as mlog

import numpy as np
from scipy.optimize import minimize

from sprout.nmnl.node import Node

log = lg.getLogger(__name__)

VAC_DATA_PATH = 'Choice-Data/Vacation-Destinations/choices-matrix.txt'
SF_WORK = 'SF_work_final'
synthetic = 'synthetic.txt'


class NMNL2:
    """
    Main class to support nmnl implementation

    """

    CHOICE = 0
    CSET = 1
    EMPTY = 2

    # initialization code
    def __init__(self, oracle, data):
        if data is None:
            data = np.zeros((2, 2))

        Node.resetCount()
        self.setData(data)

        self.cols = data.shape[1]
        self.oracle = oracle
        self.initialized = False
        self.T = Node(children=[])
        self.T.constrain()
        self.numNodes = 0
        self.numLeaves = 0
        self.numConstrained = 0

    def setData(self, data):
        unique_data, data_count = np.unique(data, axis=0, return_counts=True)

        self.unique_data = unique_data
        self.data_count = data_count
        self.data = data

    # Benson tree construction
    def constructTree(self, T, numLeaves):
        self.T = T

        self.numLeaves = numLeaves
        self.cleanTree(self.T)
        self.initConstraints(self.T)
        return self.T

    def compressTree(self, T, max_dist):
        if (T.isLeaf):
            return

        flag = True
        while flag:
            flag = False
            for child in T:
                if not child.isLeaf and T.weight - child.weight < (max_dist / 10):
                    T.children += child.children
                    T.children.remove(child)
                    flag = True

        [self.compressTree(x, max_dist) for x in T]

    # Generate data from tree
    def generate_num_data(self, num_points, num_per_choice_set, set_size):
        main_dat = self.generate_data(num_points // num_per_choice_set, num_per_choice_set, set_size)
        comp_dat = self.generate_data(1, num_points % num_per_choice_set, set_size)
        return np.concatenate((main_dat, comp_dat))

    def generate_data(self, num_choice_sets, num_per_choice_set, set_size):
        leaves = self.numLeaves
        output = np.zeros((num_choice_sets * num_per_choice_set, leaves))
        output = output - 1

        row = 0
        for i in range(num_choice_sets):
            clist = self.generate_choice_list(set_size, leaves)
            choices = self.generateChoicesFromTree(clist, num_per_choice_set)
            for choice in choices:
                output[row, np.asarray(clist) - 1] = 0
                output[row, choice - 1] = 1
                row += 1

        return output

    def generateChoicesFromTree(self, choice_array, n):
        prob_set = self.assign_probs_rec(choice_array)
        l = [self.generate_from_probability(prob_set.items()) for i in range(n)]
        return l

    def generate_choice_list(self, k, n):
        l = list(range(1, n + 1))
        random.shuffle(l)
        return l[0:k]

    def generate_from_probability(self, prob_list):
        value = random.random()
        total_prob = 0
        choice = None
        for key, weight in prob_list:
            total_prob += weight
            if value < weight:
                choice = key
                break
            else:
                value -= weight
        return choice

    def assign_probs_rec(self, choice_array, T=None):
        if T == None:
            T = self.T

        if T.isLeaf:
            if T.id in choice_array:
                return {T.id: 1}
            else:
                return None

        l = [self.assign_probs_rec(choice_array, child) for child in T]
        total = sum((T[i].exp_weight() for i in range(T.len()) if l[i] != None), 0.0)
        updated_l = [self.multiply_dict(l[key], T[key].exp_weight() / total) for key in range(len(l)) if l[key] != None]

        answer = {}
        for d in updated_l:
            answer.update(d)
        if len(answer) == 0:
            answer = None

        return answer

    def multiply_dict(self, dict, value):
        a = {k: v * value for k, v in dict.items()}
        return a

    def update_and_return(self, dict1, dict2):
        dict1.update(dict2)
        return dict1

    #### construct the tree Greedy
    def greedyConstructTree(self):
        leaves = [Node(isLeaf=True) for a in range(1, self.cols + 1)]
        self.T.setChildren(leaves)
        counter = 0
        while self.T.numChildren() > 1:
            if counter >= self.T.numChildren():
                break

            node = self.T[counter]
            pod = self.siblingSet(node, self.T)

            if len(pod) > 1:
                counter = 0
                self.T.setChildren([x for x in self.T if x not in pod])
                self.T.addChild(Node(children=pod))
            else:
                counter += 1

        self.cleanTree(self.T)
        self.initConstraints(self.T)
        return self.T

    def cleanTree(self, T):
        if T.len() == 1:
            T.setChildren(T[0].children)
        for child in T:
            self.cleanTree(child)

    def siblingSet(self, i, T):
        retset = []
        for j in T.children:
            if self.isSibling(i, j, T):
                retset.append(j)
        return retset

    def isSibling(self, i, j, T):

        for k in T:
            val = self.equal(i, j, k)
            if k != i and k != j and not val:
                return False
        return True

    def equal(self, i, j, k):
        return self.oracle.equal(self.flatten(i), self.flatten(j), self.flatten(k))

    def flatten(self, node):
        return [node.id] if node.isLeaf else [a for i in node for a in self.flatten(i)]

    def initConstraints(self, T):
        self.numNodes += 1
        if T.isLeaf:
            return

        constrainedFlag = True
        for child in T:
            if constrainedFlag:
                constrainedFlag = False
                child.constrain()
                self.numConstrained += 1
            self.initConstraints(child)

    def vectorizeParams(self, T=None):
        if T is None:
            T = self.T
        return ([T.weight] if not T.isConstrained else []) + [a for child in T for a in self.vectorizeParams(child)]

    def updateData(self, data):
        self.setData(data)

    def optimize(self, data=None):
        if not (data is None):
            self.setData(data)

        def callback(nk):
            log.debug("iteration: {}".format(callback.counter))
            callback.counter += 1
            return

        callback.counter = 0

        params = self.vectorizeParams(self.T)
        ans = minimize(lambda x: self.computeWeightsAndGradient_optim(x), params, jac=True, callback=callback, tol=1e-1)
        log.debug('Answer %s', ans)

        return self.sumNLL_optim(), ans

    def computeWeightsAndGradient_optim(self, params):

        log.debug("\n*** Start new iteration *** ")
        nll = self.sumNLL_optim(params)
        grads = np.asarray(self.getGradients(self.T))
        nf = np.asarray(self.getByFunc(self.T, lambda x: x.nf))
        grad_sum = np.asarray(self.getByFunc(self.T, lambda x: x.gradsum))

        log.debug("Grads %s", grads)
        log.debug("nll %s", nll)
        log.debug("nf %s", nf)
        log.debug("grad_sum %s", grad_sum)

        return nll, grads

    def sumNLL_optim(self, params=None, data=None):

        if data is not None:
            self.setData(data)

        if (params is not None):
            self.updateWithWeights(self.T, iter(params))

        data = self.unique_data
        data_count = self.data_count
        # data, data_count = np.unique(all_data, axis=0, return_counts=True)
        LL = 0
        reset = True
        for i in range(data.shape[0]):  # data.shape[0]

            cset = set((np.where(data[i, :] > -1)[0] + 1).flat)
            choice = np.where(data[i, :] == 1)[0] + 1
            set_count = data_count[i]

            ll_step = -set_count * self.llRec_optim(self.T, cset, choice, set_count=set_count, reset=reset)[
                1]  # the actual liklihood is the second part of the tuple
            LL += ll_step
            reset = False
        return LL

    def updateWithWeights(self, T, iter):
        T.weight = 0 if T.isConstrained else next(iter)

        if not T.isLeaf:
            for subnode in T:
                self.updateWithWeights(subnode, iter)

    def getByFunc(self, T, lam):
        ret = [] if T.isConstrained else [lam(T)]

        if not T.isLeaf:
            for subnode in T:
                ret += self.getByFunc(subnode, lam)
        return ret

    def getGradients(self, T):
        ret = [] if T.isConstrained else [T.grad()]

        if not T.isLeaf:
            for subnode in T:
                ret += self.getGradients(subnode)
        return ret

    def llRec_optim(self, node, cset, choice, set_count, reset=False):
        # If necessary, reset
        if (reset):
            node.reset()

        # Base case: leaf
        if node.isLeaf:
            id = node.id

            if id == choice:
                return self.CHOICE, 0
            elif id in cset:
                return self.CSET
            else:
                return self.EMPTY
        else:
            totalWeight = 0
            totalLL = 0
            choiceWeight = 0

            # first analyze subnodes
            flag = False

            counter = 0
            choice_set = {}
            for subnode in node:
                counter += 1
                choice_set[counter] = False

                reg_weight = subnode.weight
                exp_weight = exp(subnode.weight)

                # Get tuple from child
                recTuple = self.llRec_optim(subnode, cset, choice, set_count, reset=reset)
                if recTuple == self.EMPTY:
                    continue
                elif recTuple == self.CSET:
                    totalWeight += exp_weight
                    choice_set[counter] = True
                else:
                    totalLL += recTuple[1]
                    choiceWeight = reg_weight
                    totalWeight += exp_weight
                    subnode.incrnf(by=set_count)
                    flag = True

                    choice_set[counter] = True

            # update gradients
            counter = 0
            for subnode in node:
                counter += 1

                if totalWeight > 0 and choice_set[counter] and flag:
                    subnode.updategradsum(set_count * (1.0 / totalWeight))

            # Return appropriate response, with total LL if in choice set
            if not flag:
                return self.EMPTY if totalWeight == 0 else self.CSET
            else:
                return self.CHOICE, totalLL + choiceWeight - mlog(totalWeight)


def construct_empty():
    return NMNL2(None, None)
