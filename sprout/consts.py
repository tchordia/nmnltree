import re
from collections import namedtuple
import os

import numpy as np
from enum import Enum

### Global Experimental stuff Id

scratch_mode = True
experiment_id = 2

### default_params


path = None
log = "i"
save = True
delim = ","
default_out_path = "scratch/experiment_{}/all_output".format(experiment_id)
stats = True

class RunParameters:

    def __init__(self, alpha, uncle_alpha, max_cut_value):
        self.alpha = alpha
        self.uncle_alpha = uncle_alpha
        self.max_cut_value = max_cut_value


### Meta run items: when doing a meta run, defines how the below parameters change between runs.

id = -1
num_ids = 8


def convert(start, stop, num):
    step = round((stop - start) / num, 2)
    return np.round(np.arange(start, stop, step), 2)


grid_alpha = convert(.3, .5, 4)
grid_uncle_alpha = convert(.05, .5, 4)
grid_max_cut_value = convert(.7, 2, 4)

dir_out_format = "a_{}_ua_{}_mc_{}/"
dir_out_pattern = re.compile("a_(.*)_ua_(.*)_mc_(.*)")

### Single run information. The following items parameterize a single run.

# Directory stuff
filename_pattern = re.compile("T(\d*)_m_(\d*)_k_(\d*).*")
out_pattern = "T{}_m_{}_k_{}.data"
out_lls_pattern = "ll_out.txt"

# Training stuff
m_train = 100000
m_train_all = [1000, 5000, 10000, 15000, 20000, 30000, 50000, 70000, 100000]
trees = list(range(1, 7))

# HyperParameters
alpha = .3
uncle_alpha = .05
max_cut_value = .7


### ground truth params
ground_lls_pattern = "ground_ll.txt"


### some old deprecated stuff
default_log_dir = 'scratch/experiment_{}/log'.format(experiment_id)
default_synth_data_dir = 'res/'


##### EVERYTHING ABOVE THIS LINE IS TO BE REPLACED
# Yaml path
yaml_path = "config/default.yaml"












