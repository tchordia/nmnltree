import os

import numpy as np

import sprout.consts
import sprout.nmnl.nmnl_impl as nm
import sprout.utils as utils


def get_oracles_from_trees(tree_list, num_points=10000, num_choices=3, num_per_choice_set=50,
                           data_dir=sprout.consts.default_synth_data_dir, save=True, tree_id=None):

    """
    Main function to generate data from trees
    """

    data_list = []

    if tree_id is not None:
        tree_list = [tree_list[tree_id]]

    for tree_template in tree_list:

        num_leaves = tree_template.num_leaves
        tag_name = tree_template.tag_name

        tree = nm.construct_empty()
        tree.constructTree(tree_template, num_leaves)

        synth_dir = data_dir + '/' + str(tag_name)
        os.makedirs(synth_dir, exist_ok=True)
        synth_path = synth_dir + '/dat_'

        # generate training data for tree 1

        data = tree.generate_num_data(num_points, num_per_choice_set, num_choices)
        if save: np.savetxt(synth_path + str(num_points) + str(num_choices) + '.txt', data, fmt="%d")
        data_list.append(data)

    return data_list
