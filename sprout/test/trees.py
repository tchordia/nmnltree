import logging as lg

import sprout.utils as utils
from sprout.nmnl.nmnl_impl import Node

log = lg.getLogger(__name__)

tree_tags = ['benson', 'three_sibs', 'high_entropy', 'yash', 'large_20']


def build_tree_by_id(id):
    '''
    Construct my test trees. Now largely superseded by nick_trees.py.
    :param id:
    :type id:
    :return:
    :rtype:
    '''
    Node.resetCount()
    if id == 0:
        T = build_benson_tree()
    elif id == 1:
        nitems = 10
        base_probs = [.3, .5, .2, .6, .3, .45, .7, .25, .33, .5]
        n = [Node(isLeaf=True, weight=base_probs[i]) for i in range(nitems)]

        i = Node(children=[n[0], n[1], n[2]])
        j = Node(children=[i, n[3], n[4]])
        k = Node(children=[n[5], n[6], n[7], n[8]])

        T = Node(children=[j, k, n[9]])
        T.num_leaves = nitems
    elif id == 2:
        nitems = 10
        n = [Node(isLeaf=True) for i in range(nitems)]
        n[0].weight = 2
        n[2].weight = -2
        n[7].weight = 3
        n[8].weight = -3

        a = Node(children=[n[0], n[1], n[2], n[3], n[4]])
        b = Node(children=[n[5], n[6], n[7], n[8], n[9]])

        T = Node(children=[a, b])
        T.num_leaves = nitems
    elif id == 3:
        nitems = 10
        base_probs = [.69, .27, .69, .91, .42, .7, 0, .36, .7, .42]
        n = [Node(isLeaf=True, weight=base_probs[i]) for i in range(nitems)]

        a = Node(children=[n[0], n[1], n[2]])
        b = Node(children=[n[3], n[4], n[5], n[6]])
        c = Node(children=[b, n[7]])
        d = Node(children=[n[8], n[9]])

        T = Node(children=[a, c, d])
        T.num_leaves = nitems
    elif id == 4:
        nitems = 20
        base_probs = [.69, .27, .69, .91, .42, .7, 0.1, .36, .7, .42, .69, .27, .69, .91, .42, .7, .01, .36, .7, .42]
        n = [Node(isLeaf=True, weight=base_probs[i]) for i in range(nitems)]

        a = Node(children=[n[0], n[1], n[2]])
        b = Node(children=[n[3], n[4], n[5], n[6]])
        c = Node(children=[b, n[7]])
        d = Node(children=[n[8], n[9]])

        e = Node(children=[a, c, d])

        i = Node(children=[n[10], n[11], n[12]])
        j = Node(children=[i, n[13], n[14], n[19]])
        k = Node(children=[n[15], n[16], n[17], n[18]])

        T = Node(children=[e, j, k])

        T.num_leaves = nitems

    T.tag_name = tree_tags[id]
    return T


def build_benson_tree():
    base_probs = utils.convert_weights_to_log([.5, .2, .3, .3, .7, .2, .8, .4, .6])
    n = [Node(isLeaf=True, weight=base_probs[i]) for i in range(9)]

    parent_probs = utils.convert_weights_to_log([.3, .3, .5, .5, .4])

    i = Node(children=[n[0], n[1], n[2]], weight=parent_probs[0])
    j = Node(children=[n[3], n[4]], weight=parent_probs[1])
    k = Node(children=[n[5], n[6]], weight=parent_probs[2])
    l = Node(children=[n[7], n[8]], weight=parent_probs[3])

    r = Node(children=[k, l], weight=parent_probs[4])

    T = Node(children=[i, j, r])
    T.num_leaves = 9
    return T


def build_tree_from_template(template, nitems):
    Node.resetCount()
    n = [Node(isLeaf=True) for i in range(nitems)]

    internal_nodes = {}

    last = None

    for id, merge_list in template:
        if id != Node.nextInnerId:
            raise RuntimeError

        internal_nodes[id] = Node(children=[n[i] if i >= 0 else internal_nodes[i] for i in merge_list])
        last = internal_nodes[id]

    return last


def convert_to_tuple_map(tree, dict):
    dict[tuple(sorted([child.id - 1 if child.id > 0 else child.id for child in tree.children]))] = tree.id
    for child in tree.children:
        convert_to_tuple_map(child, dict)


def match_tree_to_template(tree, template):
    """
    Check if a particular tree matches a particular template. One way to check if they are equal (without optimizing).
    :param tree:
    :type tree:
    :param template:
    :type template:
    :return:
    :rtype:
    """
    tree_map = {}
    convert_to_tuple_map(tree, tree_map)

    conversion_map = {}
    grab_tuple = lambda list: tuple(sorted([conversion_map[k] if k in conversion_map else k for k in list]))

    for id, merge_list in template:
        new_key = grab_tuple(merge_list)
        if new_key in tree_map:
            conversion_map[id] = tree_map[new_key]
        else:
            return False
    return True
