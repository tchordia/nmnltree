import logging as lg

import sprout.utils as utils
from sprout.nmnl.nmnl_impl import Node
from sprout.test.trees import build_benson_tree

log = lg.getLogger(__name__)

def build_tree_by_id(id):
    """
    Build any of Nick's test trees, by tree id. These functions simply defer to the below functions that actually build
    the tree

    :param id:
    :type id:
    :return:
    :rtype:
    """
    Node.resetCount()
    options = {
        1: buildT1,
        2: buildT2,
        3: buildT3,
        4: buildT4,
        5: buildT5,
        6: buildT6
    }

    return options[id]()


def buildT1():
    return build_benson_tree()


def buildT2():
    nitems = 7
    base_probs = utils.convert_weights_to_log([.1, .2, .15, .15, .1, .2, .1])
    n = [Node(isLeaf=True, weight=base_probs[i]) for i in range(nitems)]
    T = Node(children=n)
    T.numleaves = nitems
    return T


def buildT3():
    nitems = 5
    base_probs = utils.convert_weights_to_log([.2, .3, .5, .3, .7])
    parent_probs = utils.convert_weights_to_log([.4, .6])

    n = [Node(isLeaf=True, weight=base_probs[i]) for i in range(nitems)]
    p0 = Node(children=n[:3], weight=parent_probs[0])
    p1 = Node(children=n[3:], weight=parent_probs[1])

    T = Node(children=[p0, p1])
    T.numleaves = nitems
    return T


def buildT4():
    nitems = 20
    base_probs = utils.convert_weights_to_log(
        [.2, .3, .5, .3, .7, .1, .2, .15, .15, .4, .1, .2, .15, .15, .1, .2, .1, .6, .1, .3])
    parent_probs = utils.convert_weights_to_log([.3, .2, .2, .1, .2])

    n = [Node(isLeaf=True, weight=base_probs[i]) for i in range(nitems)]
    p0 = Node(children=n[:3], weight=parent_probs[0])
    p1 = Node(children=n[3:5], weight=parent_probs[1])
    p2 = Node(children=n[5:10], weight=parent_probs[2])
    p3 = Node(children=n[10:17], weight=parent_probs[3])
    p4 = Node(children=n[17:], weight=parent_probs[4])

    T = Node(children=[p0, p1, p2, p3, p4])
    T.numleaves = nitems
    return T


def buildT5():
    base_probs = utils.convert_weights_to_log(
        [.3, .3, .4, .2, .1, .9, .3, .7, .5, .5, .3, .7, .4, .5, .5, .2])
    nitems = len(base_probs)
    parent_probs = utils.convert_weights_to_log([.5, .3, .3, .2, .8, .8])
    q_probs = utils.convert_weights_to_log([.3, .5, .6])
    o_probs = utils.convert_weights_to_log([.5, .4])

    n = [Node(isLeaf=True, weight=base_probs[i]) for i in range(nitems)]

    p0 = Node(children=n[:3], weight=parent_probs[0])
    p1 = Node(children=n[4:6], weight=parent_probs[1])
    p2 = Node(children=n[6:8], weight=parent_probs[2])
    p3 = Node(children=n[8:10], weight=parent_probs[3])
    p4 = Node(children=n[10:12], weight=parent_probs[4])
    p5 = Node(children=n[13:15], weight=parent_probs[5])

    q1 = Node(children=[p0, n[3], p1], weight=q_probs[0])
    q2 = Node(children=[p3, p4], weight=q_probs[1])
    q3 = Node(children=[p5, n[15]], weight=q_probs[2])

    r = Node(children=[q3, n[12]], weight=o_probs[0])

    s = Node(children=[q2, r], weight=o_probs[1])

    T = Node(children=[q1, p2, s])
    T.numleaves = nitems
    return T


def buildT6():
    nitems = 9
    base_probs = utils.convert_weights_to_log([.3, .5, .1, .4, .3, .2, .9, .5, .5])
    n = [Node(isLeaf=True, weight=base_probs[i]) for i in range(nitems)]
    parent_probs = utils.convert_weights_to_log([0, .7, .5, .9, .6, .7, .8, .1, .5])

    t = n[-1]
    for i in reversed(range(nitems - 1)):
        t = Node(children=[n[i], t], weight=parent_probs[i])

    T = t
    T.numleaves = nitems
    return T


if __name__ == "__main__":
    build_tree_by_id(6)
