import math
import numpy as np
from math import log
import larch
import matplotlib.pyplot as plt
import networkx as nx

# Overall layout: first the
# Given an Oracle, construct a Tree

def buildTreeRecursive(T, model, parent_id):
    if isLeaf(T):
        return T

    my_id = model.new_node(str(T), parent=parent_id)
    sublist = []
    for subt in T:
        subid = buildTreeRecursive(subt, model, my_id)
        sublist.append(subid)
        model.link(my_id, subid)

    print(sublist)
    return my_id


def isLeaf(node):
    return type(node) == type(1)


#
# dataset = larch.DB.Example('MTC')
# model = larch.Model(dataset)

dataset = larch.DB.Example('SWISSMETRO')
model = larch.Model(dataset)
T = {1,2,3,4,5,6}
id = buildTreeRecursive(T, model, model.root_id)
# model.link(model.root_id, id)
nx.draw(model.graph)
plt.show()
m = larch.Model.Example(109)