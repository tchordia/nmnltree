import collections

import numpy as np
import pandas as pd
import scipy.spatial.distance as ssd
from scipy.cluster.hierarchy import *

from sprout.nmnl import NMNL2
from sprout.nmnl import Node
from sprout.nmnl import oracle

isChisq = True

tree_paths = ['synthetic/', 'synthetic_2/', 'synthetic_3/', 'synthetic_4/']
test_paths = ['test/','test_2/', 'test_3/','test_4/']

num_datapoints = [500,1000,5000,10000]
choice_set_size = [3, 7]
choice_set_tests = ['3','7','10','mixed']
num_leaves = 10

conv = {0: '3', 1: '7', 2:'10', 3:'mixed'}

def convertToNmnlTreeRec(tree):

    if tree is None:
        return

    node = Node(isLeaf = tree.is_leaf(), weight=tree.dist)
    if not tree.is_leaf():
        if not (tree.get_left() is None):
            node.addChild(convertToNmnlTreeRec(tree.get_left()))

        if not (tree.get_right() is None):
            node.addChild(convertToNmnlTreeRec(tree.get_right()))
    return node

def getSet(data_path, i, c):
    path = data_path + 'dat_' + str(num_datapoints[i]) + str(choice_set_size[c]) + '.txt'
    X = np.loadtxt(path)
    return X

def getTestSet(i):
    path = 'test/test_' + str(choice_set_tests[i])
    return np.loadtxt(path)

def save_data(insampleLL, outsampleLL, tree_num, training_choice_set, is_chisq):
    l = [v for k, v in insampleLL.items()]
    df = pd.DataFrame(outsampleLL)
    df = df.rename(columns=conv)

    df['Num Training'] = num_datapoints
    df['insample'] = l

    cols = df.columns.tolist()

    cols.insert(0, 'insample')
    cols.insert(0, 'Num Training')

    del cols[-1]
    del cols[-1]

    df = df[cols]
    tag = 'chisq' if is_chisq else 'metric'
    print(df)
    df.to_csv('output_file_' + tag + '/tree_' + str(tree_num) + '_' + str(training_choice_set) + '.txt', float_format="%d", index=False)


def getMetTreeData(X):
    oracle = oracle(X)
    dist = oracle.getMatVect() if not isChisq else oracle.getMatVect_chi2()
    distArray = ssd.squareform(dist)
    Z = linkage(distArray, method='weighted')
    tree = to_tree(Z)
    Node.resetCount()
    T = convertToNmnlTreeRec(tree)
    nmnl = NMNL2.fromDendoTree(T, num_leaves, X)
    return nmnl

def getGreedyTreeData(X):
    oracle = oracle(X)
    Node.resetCount()
    nmnl = NMNL2(oracle, X)
    nmnl.greedyConstructTree()
    return nmnl

for i in range(len(tree_paths)):
    path = tree_paths[i]
    tree_num = i + 1
    for size_idx in range(len(choice_set_size)):
        insampleLL = collections.OrderedDict()
        outsampleLL = []
        size = choice_set_size[size_idx]
        full_data = getSet(path, 3, size_idx)

        for i in num_datapoints:
            X = full_data[0:i, :]
            nmnl = getMetTreeData(X)

            insampleLL[i] = nmnl.optimize(X)

            ll_dict = {}
            for i in range(len(choice_set_tests)):
                test = getTestSet(i)
                nmnl.updateData(test)
                ll_dict[i] = nmnl.sumNLL()

            outsampleLL.append(ll_dict)

        save_data(insampleLL, outsampleLL, tree_num=tree_num, training_choice_set=size, is_chisq=isChisq)


