import pandas as pd
import matplotlib.pyplot as plt

trees = ["1", "2", "3", "4"]
cs = ["3", "7"]
test_csets = ["3", "7", "10", "mixed"]
insample = "insample"
tags = ["chisq", "metric", "oracle"]

def get_fname(tree, cset_size):
    return "tree_" + tree + "_" + cset_size + ".txt"

def get_path(tag, tree, cset_size):
    return "output_file_" + tag + "/" + get_fname(tree, cset_size)

for tree in trees:
    for cset_size in cs:
        paths = [get_path(tag, tree, cset_size) for tag in tags]
        frames = [pd.read_csv(path) for path in paths]
        df = pd.DataFrame()

        nt = frames[0]["Num Training"]

        for test_cset in test_csets:

            for idx, tag in enumerate(tags):
                df[tag] = frames[idx][test_cset]
            title = "Tree: " + tree + ", Training Cset: " + cset_size + ", Testing cset: " + test_cset
            sbplot = df.plot(title = title,x= nt)
            fig = sbplot.get_figure()
            fig.savefig("plots/plot_t" + tree + "_c" + cset_size + "_tc" + test_cset + ".png")

        # plt.plot(frames[0]["Num Training"], frames[0]["3"])
        # plt.plot(frames[0]["Num Training"], frames[1]["3"])
        # plt.plot(frames[0]["Num Training"], frames[2]["3"])
        # plt.show()


