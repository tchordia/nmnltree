# Given the data, construct an oracle
import os

import numpy as np

from sprout.nmnl import nmnl_impl as nm
from sprout.nmnl.node import Node


VAC_DATA_PATH = 'Choice-Data/Vacation-Destinations/choices-matrix.txt'
SF_WORK = 'SF_work_final'
synthetic = 'synthetic.txt'

nitems = 10
num_elem_per = 30
choice_set_size = [3, 7]
test_set_size = [3, 5, 7, 10]
number_data_points_total = [100000]

def tree_1():
    n = [Node(isLeaf=True) for i in range(nitems)]

    i = Node(children=[n[0], n[1], n[2]])
    j = Node(children=[i, n[3], n[4]])
    k = Node(children=[n[5], n[6], n[7], n[8]])

    T = Node(children=[j, k, n[9]])

    return T

def tree_TEST():
    n = [Node(isLeaf=True) for i in range(9)]

    i = Node(children=[n[0], n[1], n[2], n[3], n[4]])
    r = Node(children=[i, n[5], n[6], n[7], n[8]])


    T = Node(children=[r])

    return T

def convert(list):
    vect = np.asarray(list)
    vect = np.log(vect)
    return vect

def tree_FUNKY():

    base_probs = convert([.5, .2, .3, .3, .7, .2, .8, .4, .6])
    n = [Node(isLeaf=True, weight=base_probs[i]) for i in range(9)]

    parent_probs = convert([.3, .3, .5, .5, .4])

    i = Node(children=[n[0], n[1], n[2]], weight = parent_probs[0] )
    j = Node(children=[n[3], n[4]], weight = parent_probs[1])
    k = Node(children=[n[5], n[6]], weight = parent_probs[2])
    l = Node(children=[n[7], n[8]], weight = parent_probs[3])

    r = Node(children=[k, l], weight = parent_probs[4])

    T = Node(children=[i, j, r])

    return T

def tree_2():
    n = [Node(isLeaf=True) for i in range(nitems)]

    i = Node(children=[n[0], n[1], n[2]])
    j = Node(children=[n[3], n[4]])
    k = Node(children=[n[5], n[6], n[7], n[8]])

    T = Node(children=[i, j, k, n[9]])
    return T


def tree_3():
    n = [Node(isLeaf=True) for i in range(nitems)]

    a = Node(children=[n[0], n[1], n[2], n[3], n[4]])
    b = Node(children=[n[5], n[6], n[7], n[8], n[9]])

    T = Node(children=[a, b])

    return T


def tree_4():
    n = [Node(isLeaf=True) for i in range(nitems)]

    n[0].weight = 2
    n[2].weight = -2
    n[7].weight = 3
    n[8].weight = -3

    a = Node(children=[n[0], n[1], n[2], n[3], n[4]])
    b = Node(children=[n[5], n[6], n[7], n[8], n[9]])

    T = Node(children=[a, b])

    return T


def get_tree(list, i):
    tree = nm.construct_empty()
    tree.constructTree(list[i - 1], nitems)
    return tree

def gen_tree_TEST(n=300000, data_dir='res/data'):

    num_choices = 3

    tree = nm.construct_empty()
    tree.constructTree(tree_TEST, 9)

    print(str(tree.T))
    input("...")

    synth_dir = data_dir + '/synthetic_TEST_5n5'
    os.makedirs(synth_dir, exist_ok=True)
    synth_path = synth_dir + '/dat_'

    # generate training data for tree 1

    data = tree.generate_num_data(n, 50, num_choices)
    np.savetxt(synth_path + str(n) + str(num_choices) + '.txt', data, fmt="%d")

def gen_tree_FUNKY(n=30000, data_dir='res/data'):

    num_choices = 3

    tree = nm.construct_empty()
    tree.constructTree(tree_FUNKY, 9)

    print(str(tree.T))
    input("...")

    synth_dir = data_dir + '/synthetic_FUNKY'
    os.makedirs(synth_dir, exist_ok=True)
    synth_path = synth_dir + '/dat_'

    # generate training data for tree 1

    data = tree.generate_num_data(n, 50, num_choices)
    np.savetxt(synth_path + str(n) + str(num_choices) + '.txt', data, fmt="%d")

def generate_data(num_test_points=5000, data_dir='res/data'):
    treelist = [tree_1, tree_2, tree_3, tree_4]

    for tree_num in range(len(treelist)):

        tree = get_tree(treelist, tree_num)

        test_dir = data_dir + '/test_' + str(tree_num)
        synth_dir = data_dir + '/synthetic_' + str(tree_num)


        os.makedirs(test_dir, exist_ok=True)
        os.makedirs(synth_dir, exist_ok=True)

        test_path = test_dir + '/test_'
        synth_path = synth_dir + '/dat_'

        # generate training data for tree 1
        for n in number_data_points_total:
            for i in choice_set_size:
                data = tree.generate_num_data(n, num_elem_per, i)
                np.savetxt(synth_path + str(n) + str(i) + '.txt', data, fmt="%d")

        # generate test data for tree 1
        for i in test_set_size:
            data = tree.generate_num_data(num_test_points, num_elem_per, i)
            np.savetxt(test_path + str(i), data, fmt="%d")

        data = None
        num_mix = len(test_set_size)

        for i in test_set_size:
            if data is None:
                data = tree.generate_num_data(n // num_mix, num_elem_per, i)
            else:
                data = np.concatenate((data, tree.generate_num_data(n // num_mix, num_elem_per, i)))

        np.savetxt(test_path + 'mixed', data, fmt="%d")

gen_tree_TEST()