import math
import numpy as np
from scipy.optimize import minimize


# do MLE on the data as follows: each y is id number of the vacation destination that was chosen
# Probability that the ith person picks the jth object when shown S: P(X_i = x_j | w, S) = w_j / sum (w_(j in S)
# aka: show objects 1, 2, 3, 4; probability of choosing first is w1/(w1 + w2 + w3 + w4)
# LL =

VAC_DATA_PATH = 'Choice-Data/Vacation-Destinations/choices-matrix.txt'
SF_WORK = 'SF_work_final'


def powerSet(n):  # Find all subsets of size n
    powerSet = list()  # list of lists
    get_bin = lambda x: format(x, 'b')
    num_sets = math.pow(2, n)
    stringSize = len(get_bin(int(num_sets)-1))

    for i in range(0, int(num_sets)):
        subSet = list()
        binary = get_bin(i)
        while len(binary) < stringSize:
            binary = str(0) + binary
        for index, character in enumerate(binary):
            if character == '1':
                subSet.append(index+1)
        powerSet.append(subSet)

    return powerSet

class MNL:

    def __init__(self):
        pass

    def get_loss(self, data):
        all = (data > -1).astype(int);
        winner = (data == 1).astype(int);
        loss = lambda w : -np.sum(np.log(np.divide(np.dot(winner, w), np.dot(all, w))))
        return loss

    def compute_MNL(self, data):
        all = (data > -1).astype(int);
        winner = (data == 1).astype(int);



        # MNL MLE implementation

        loss = lambda w : -np.sum(np.log(np.divide(np.dot(winner, w), np.dot(all, w))))

        num_fields = data.shape[1]
        weights = np.ones(num_fields) / num_fields ;


        print("initial " + str(loss(weights)))

        constraint = {'type': 'eq', 'fun': lambda w: np.sum(w) - 1}
        bds = [(0.001, None)] * num_fields
        res = minimize(loss, weights, bounds=bds, constraints=constraint)
        w_hat = res.x
        self.w_hat = w_hat
        self.inner_loss = loss(w_hat)
        print(w_hat)
        print(res)
        return (w_hat, self.inner_loss)

    def compute_dirichlet(self, data):
        all = (data > -1).astype(int)
        winner = (data == 1).astype(int)
        loss = lambda w: -np.sum(np.log(np.divide(np.dot(winner, w), np.dot(all, w) )))
        loss2 = lambda w: np.dot(winner, w) + 1

        num_fields = data.shape[1]
        weights = np.ones(num_fields)
        print(loss2(weights))
        bds = [(0.0001, None)] * num_fields
        res = minimize(loss, weights, bounds=bds)
        return res.x, loss(res.x)






    def test_outer_loss(self, data):
        loss = self.get_loss(data)
        return loss(self.w_hat)




    #compute in sample error

    # val = np.tile(w_hat.T, (num_rows,1))
    # pred = np.argmax(val * all, axis=1)
    # y = np.argmax(winner, axis=1)
    # acc = np.mean((pred == y).astype(int))

fulldata = np.loadtxt('data/synthetic_0/T1_m_100000_k_3.txt')
# test = np.loadtxt('test/test_3')

mnl = MNL()
print(mnl.compute_dirichlet(fulldata[1:10000,:]))
print(mnl.compute_MNL(fulldata[1:10000,:]))

