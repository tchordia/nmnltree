import numpy as np

data_arr = np.genfromtxt('Choice-Data/Fields-of-Study/choices-matrix.txt', dtype=None, delimiter=' ')
a = (data_arr>-1) - 1 + (data_arr == 1)
np.savetxt('fieldsofstudy.txt', a, fmt="%d")