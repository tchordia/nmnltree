from scipy import stats
import numpy as np
import unittest

data = np.loadtxt('res/data/synthetic_TEST/T1_m_100000_k_3.txt')
cols = 6
alpha = .05
sets = {}

def test_data_is_valid(self):
    for k in range (1, 10):
        sample_size = 50
        n = 4

        start = sample_size * k

        sample1 = data[start: start + sample_size, :]

        # print(sample1)

        observed = np.sum(sample1, axis=0)
        print(observed)

        expected = np.ones(n)  * sample_size / n
        print(expected)

        ind = np.where(observed <= 0)[0]

        map = {0: 1, 1: 0, 3:2, 2:3}

        expected[ind] = 1
        expected[map[ind[0]]] = .5 * sample_size
        observed[observed <= 0] = 1

        chiray = np.asarray([expected, observed])

        print(chiray)

        chi2, pval, _, _ = stats.chi2_contingency(chiray)

        print(chi2, pval)


def compute_list(i, j, k, p_ij):
    pres = np.prod(data[:, [i, j , k]] >= 0, 1)

    wins_i = np.sum(pres * (data[:, i] > 0)) + 1.0
    wins_j = np.sum(pres * (data[:, j] > 0)) + 1.0
    expected = (wins_i + wins_j) * p_ij

    return [wins_i, expected], [wins_j, wins_i + wins_j - expected]


def compute_pij(i , j):
    pres = np.prod(data[:, [i, j]] >= 0, 1)
    wins_i = np.sum(pres * (data[:, i] > 0)) + 1.0
    wins_j = np.sum(pres * (data[:, j] > 0)) + 1.0
    return wins_i/(wins_j + wins_i)

def compute_chi_sq(i, j):
    pij = compute_pij(i,j)

    pair = [compute_list(i, j, k, pij) for k in range(cols) if k != i and k != j and data[1,k] is not np.nan]

    t1 = [x[0] for x in pair]
    t2 = [x[1] for x in pair]

    l1 = np.asarray(t1)
    l2 = np.asarray(t2)

    chi21, pval1, _, _ = stats.chi2_contingency(l1)
    chi22, pval2, _, _ = stats.chi2_contingency(l2)



    distance = round(1 - (pval1 * pval2) ** .5, 3)

    if (distance < 1 - alpha):

        set_i = {i}
        set_j = {j}

        if (i in sets):
            set_i = sets[i]

        if (j in sets):
            set_j = sets[j]

        if (set_i != set_j):
            set_i = set_i | set_j
            sets[i] = set_i
            sets[j] = set_i


    return distance

def getMatVect_chi2():
    mat = np.asarray([[compute_chi_sq(i, j) for j in range(cols)] for i in range(cols)])

    return mat

def p_ijk(i, j, k):
    pres = np.prod(data[:, [i, j, k]] >= 0, 1)

    count = np.sum(pres) + 3
    wins_i = np.sum(pres * (data[:, i] > 0)) + 1.0
    wins_j = np.sum(pres * (data[:, j] > 0)) + 1.0
    wins_k = np.sum(pres * (data[:, k] > 0)) + 1.0
    return wins_i/count, wins_j/count, wins_k/count, count, wins_i/(wins_j + wins_i)


def isUncle(equivSet1, equivSet2):

    iter1 = iter(equivSet1)
    iter2 = iter(equivSet2)

    i = next(iter1)
    j = next(iter2)

    k1 = next(iter2)
    k2 = next(iter1)

    p_i1, p_j1, p_k1, count, p_ijk1 = p_ijk(i, j, k1)
    p_i2, p_j2, p_k2, count, p_ijk2 = p_ijk(i, j, k2)

    p_ij = compute_pij(i, j)
    diff = (p_ijk1 - p_ij) /  ((1 - p_ijk2) - (1 - p_ij) )

    print(i, j , k1, k2)
    print(p_i1, p_j1, p_k1)
    print(p_i2, p_j2, p_k2)


    uncle_test = np.asarray([
        [p_i2, (p_i1 * (1 - p_k2))],
        [p_j2, (p_j1 + p_k1) * (1 - p_k2)]
    ]) * count

    peer_test = np.asarray([
        [p_i2, p_i1 - p_k2],
        [p_j2, p_j1 + p_k1]
    ]) * count


    print('diff', )

    print('values ', p_ijk1, p_ij, 1 - p_ijk2)
    print(uncle_test)
    print(peer_test)

    chi21, pval_uncle, _, _ = stats.chi2_contingency(uncle_test)
    chi21, pval_peer, _, _ = stats.chi2_contingency(peer_test)

    unclePresent = pval_uncle > alpha and pval_uncle > pval_peer


    return None if not unclePresent else (equivSet1 if diff > 1 else equivSet2)

# mat = getMatVect_chi2()
#
# print(mat)
# print(sets)
#
# print('Result ', isUncle(sets[0], sets[2]))
# print('Result ', isUncle(sets[0], sets[4]))
# print('result ', isUncle(sets[2], sets[4]))
#
#
# print('Result ', isUncle(sets[2], sets[0]))
# print('Result ', isUncle(sets[4], sets[0]))
# print('result ', isUncle(sets[4], sets[2]))
#
# n = compute_chi_sq(0, 2)



def convert(list):
    vect = np.asarray(list)
    vect = np.log(vect)
    return vect

def convertBack(vect):
    nvect = np.exp(vect)
    return nvect / sum(nvect)

print(convert([.5, .2, .3]))
print(convertBack(convert([.5, .2, .3])))




