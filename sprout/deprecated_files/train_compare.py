import collections
import os

import dill
import numpy as np
import pandas as pd
import scipy.spatial.distance as ssd
from scipy.cluster.hierarchy import *

from sprout.nmnl import NMNL2
from sprout.nmnl import Node
from sprout.nmnl import oracle

isChisq = False

tree_paths =['synthetic_3/'] #['synthetic_0/', 'synthetic_1/', 'synthetic_2/', 'synthetic_3/']

num_datapoints = [500, 1000, 5000, 10000]
choice_set_size = [3, 7]
choice_set_tests = ['3', '7', '10', 'mixed']
num_leaves = 10

conv = {0: '3', 1: '7', 2: '10', 3: 'mixed'}

dp = 'data/'

tags = ['chisq', 'metric', 'oracle']
CHISQ = 0
METRIC = 1
ORACLE = 2

tag = CHISQ

def convertToNmnlTreeRec(tree):
    if tree is None:
        return

    node = Node(isLeaf=tree.is_leaf(), weight=tree.dist)
    if not tree.is_leaf():
        if not (tree.get_left() is None):
            node.addChild(convertToNmnlTreeRec(tree.get_left()))

        if not (tree.get_right() is None):
            node.addChild(convertToNmnlTreeRec(tree.get_right()))
    return node


def getSet(data_path, c):
    path = data_path + 'dat_' + str(100000) + str(choice_set_size[c]) + '.txt'
    X = np.loadtxt(path)
    return X


def getTestSet(tree_num, i):
    path = dp + 'test_' + str(tree_num - 1) + '/test_' + str(choice_set_tests[i])
    return np.loadtxt(path)


def save_data(insampleLL, outsampleLL, tree_num, training_choice_set, tag):
    l = [v for k, v in insampleLL.items()]
    df = pd.DataFrame(outsampleLL)
    df = df.rename(columns=conv)

    df['Num Training'] = num_datapoints
    df['insample'] = l

    cols = df.columns.tolist()

    cols.insert(0, 'insample')
    cols.insert(0, 'Num Training')

    del cols[-1]
    del cols[-1]

    df = df[cols]
    print(df)
    # input('pause')

    path = 'output_file_' + tag + '/'
    mkdr(path)
    df.to_csv(path + 'tree_' + str(tree_num) + '_' + str(training_choice_set) + '.txt',
              float_format="%d", index=False)


def getMetTreeData(X, chq = True):
    oracle = oracle(X)
    dist = oracle.getMatVect() if not chq else oracle.getMatVect_chi2()
    distArray = ssd.squareform(dist)
    Z = linkage(distArray, method='weighted')
    tree = to_tree(Z)
    Node.resetCount()
    T = convertToNmnlTreeRec(tree)
    nmnl = NMNL2.fromDendoTree(T, num_leaves, X)
    return nmnl


def getGreedyTreeData(X):
    oracle = oracle(X)
    Node.resetCount()
    nmnl = NMNL2(oracle, X)
    nmnl.greedyConstructTree()
    return nmnl

def mkdr(path):
    os.makedirs(path, exist_ok=True)


def save_tree(model, tree_num, choice_set_size, num_training_pts):
    path = 'models/model_t'+ str(tree_num) + '_c' + str(choice_set_size) + '_tp' + str(num_training_pts) + '.pkl'
    mkdr('models')
    print('Saving model', tree_num, choice_set_size, num_training_pts)
    with open(path, 'wb') as f:
        dill.dump(model, f)

for k in range(len(tree_paths)):
    path = dp + tree_paths[k]
    tree_num = 4
    print('Starting Tree', tree_num)
    for size_idx in range(len(choice_set_size)):
        insampleLL = collections.OrderedDict()
        outsampleLL = []
        size = choice_set_size[size_idx]
        full_data = getSet(path, size_idx)
        print('Starting dataset', size)

        for i in num_datapoints:
            print('Num datapoints', i)
            X = full_data[0:i, :]
            nmnl = getGreedyTreeData(X) if tag == ORACLE else getMetTreeData(X, chq=(tag == CHISQ))

            insampleLL[i] = nmnl.optimize(X)

            ll_dict = {}
            for si in range(len(choice_set_tests)):
                test = getTestSet(tree_num, si)
                nmnl.updateData(test)
                ll_dict[si] = nmnl.sumNLL()

            outsampleLL.append(ll_dict)
            save_tree(nmnl, tree_num, size, i)

        save_data(insampleLL, outsampleLL, tree_num=tree_num, training_choice_set=size, tag=tags[tag])
