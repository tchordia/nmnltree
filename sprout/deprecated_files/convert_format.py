import pandas as pd
import numpy as np

mtc = pd.read_csv("MTCwork.csv")
grouped = mtc.groupby("casenum")

out = np.ones((len(grouped), 6), dtype=np.int) * -1

for name, group in grouped:
    altnum = group['altnum']
    choose = group['chose']

    for index, alt in altnum.iteritems():
        choice = choose[index]
        out[name - 1, alt-1] = choice

print(out)
np.savetxt("convSFwork.txt", out, delimiter=" ", fmt="%d")

