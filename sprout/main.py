import argparse
import cProfile
import logging
import pstats

import sprout.run as run
from sprout.configerator import FilesObj
from sprout.configerator import config

lg = logging.getLogger(__name__)


def add_boolean_flag(parser, str, default):
    save_parser = parser.add_mutually_exclusive_group(required=False)
    save_parser.add_argument('--{}'.format(str), dest=str, action='store_true')
    save_parser.add_argument('--no{}'.format(str), dest=str, action='store_false')
    parser.set_defaults(**{str: default})


def main():
    '''

    The main function that serves as an entrypoint.
    Handles argument parsing, and passes all information to run.py file.

    :return:
    :rtype:
    '''

    # Declare all arguments here
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', default=None)
    parser.add_argument('-l', '--log', default="i")
    parser.add_argument('-d', '--delim', default=",")
    parser.add_argument('-i', '--id', default=1, type=int)
    parser.add_argument('-n', '--num_ids', default=1, type=int)
    parser.add_argument('-e', '--exp_tag', default="default")
    add_boolean_flag(parser, 'save', True)
    add_boolean_flag(parser, 'stats', False)
    add_boolean_flag(parser, 'scratch', True)

    namespace = parser.parse_args()

    set_log(namespace)

    config.runtime = namespace
    config.current = config.exp[namespace.exp_tag]
    config.current_tag = namespace.exp_tag
    config.files = FilesObj(namespace.scratch, raw_input_path=namespace.path)

    # If we want profile stats, grab them, otherwise run directly
    if namespace.stats:
        cProfile.run('run_tests()', config.files.profile_path)
        lg.info("Printing profile information")
        p = pstats.Stats('restats')
        p.strip_dirs().sort_stats('time').print_stats(10)
    else:
        run_tests()


def set_log(namespace):
    log_dict = {
        "d": logging.DEBUG,
        "w": logging.WARNING,
        "e": logging.ERROR,
        "i": logging.INFO,
        "c": logging.CRITICAL
    }
    logging.basicConfig(level=log_dict[namespace.log])


def run_tests():
    run.main_test()


if __name__ == "__main__":
    main()
