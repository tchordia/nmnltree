"""
This file contains a lot of utility functions that are used throughout the remainder of the codebase

"""

import numpy as np

import re
from sprout.configerator import config

def convert_weights_to_log(list):
    vect = np.asarray(list)
    vect = np.log(vect)
    return vect


def parse_filename(str):
    a = re.compile(config.current.input.filename_regex).match(str)
    return a.group(1), a.group(2), a.group(3)

@DeprecationWarning
def equals(node1, node2):
    """
    Checks if two nodes have the same children and the same weight.
    Unused in the current code.

    :param node1:
    :type node1:
    :param node2:
    :type node2:
    :return:
    :rtype:
    """

    if (node1.weight != node2.weight or len(node1.children) != len(node2.children)):
        return False

    children_dict = {}

    for child in node1.children:
        children_dict[child.id] = child

    for child in node2.children:
        other_child = children_dict.pop(child.id, None)
        if other_child is None or not equals(child, other_child):
            return False

    if children_dict:
        return False

    return True








