import itertools
import logging
import os
import pickle
import re
import traceback
from collections import namedtuple

import numpy as np
import pandas as pd

import sprout.configerator as configerator
import sprout.consts as consts
import sprout.test.generate_synthetic_data as gen_oracle
import sprout.test.trees as trees
from sprout.configerator import config
from sprout.nmnl.nmnl_impl import NMNL2
from sprout.nmnl.oracle import Oracle

log = logging.getLogger(__name__)


def main_test():
    '''
    The main function that runs all tests. The data path is passed in as namespace.path

    :param namespace: The object containing all of the input parameters, passed in from main
    :type namespace: namespace object
    :return: None
    :rtype: None
    '''
    log.info("Starting main test...")

    main_execute_meta()

    log.info("Main run complete!")


def main_execute_meta():
    '''
    Runs a single iteration of training and testing the model on all test
    files with a certain parameter setting.

    Does the following:

    loop through all files in data directory:
        extract information about the file
        call run_pre_data_test()

    :param data_dir: The data directory
    :type data_dir: string
    :param save: whether or not to save checkpoints /
    :type save:
    :return:
    :rtype:
    '''

    data_dir = config.files.raw_data_path
    m_train_all = config.current.m_train_all
    RunParams = namedtuple("RunParams", ["data", "alpha", "uncle_alpha", "max_cut", "m_train_all"])
    all_out = []

    log.info("Loading training files from %s", data_dir)

    def parse(str):
        a = re.compile(config.current.input.filename_regex).match(str)
        return a.group(1), a.group(2), a.group(3)

    def get_file_checkpoint_ext(file_tup):
        return config.current.output.checkpoint.format(*file_tup)

    def get_file_data_ext(file_tup):
        return config.current.output.data.format(*file_tup)

    def write_out_data(data_list, path):
        df_tree = pd.DataFrame(data_list)
        df_tree.columns = ['T', 'nc', 'm', 'll', 'alpha', 'uncle_alpha', 'mincut']
        df_tree.to_csv(path)

    for filename in os.listdir(data_dir):

        log.info("\nTesting new file %s", filename)

        tree_num, _, choice_num = file_tup = parse(filename)
        checkpoint_ext = get_file_checkpoint_ext(file_tup)

        # ensure we want to test this tree
        if not (int(tree_num) in config.current.tree_ids and int(choice_num) in config.current.choice_ids):
            continue

        data = None
        try:
            data = np.loadtxt(data_dir + filename, delimiter=config.runtime.delim)
        except Exception as e:
            log.error("Caught an exception trying to load the file! Skipping...")
            continue



        tree_out = []

        log.info("Tree number: %s, Num choice sets %s", tree_num, choice_num)

        for param_tup in configerator.grab_meta_iterator():

            # Define necessary variables
            params_adder = config.current.output.params_output.format(
                *param_tup
            )
            check_out_path = config.files.checkpoint_path + params_adder + checkpoint_ext
            run_tup = (data,) + param_tup + (m_train_all,)
            runparams = RunParams(*run_tup)

            # log stuff
            log.info("\n*** Testing with new hyper params*** alpha: {}, uncle alpha: {}, max cut: {}"
                     .format(*param_tup))

            # if we're saving stuff and the output file exists, then skip
            if config.runtime.save and os.path.isfile(check_out_path):
                log.info("Skipping because ouput exists...")
                continue

            log.info("Checkpoint path: {}".format(check_out_path))

            # Run
            out, t_lls = main_run_single(runparams)
            tree_out += [(tree_num, choice_num, m, ll) + param_tup for m, ll in zip(m_train_all, t_lls)]

            if config.runtime.save:
                dump(out, check_out_path)

        raw_out_path = config.files.raw_output_path + get_file_data_ext(file_tup)
        log.info("Writing data for tree to %s", raw_out_path)
        write_out_data(tree_out, raw_out_path)
        all_out += tree_out

    log.info("Writing full data to %s", config.files.final_data_path)
    write_out_data(all_out, config.files.final_data_path)


def main_run_single(rp):
    '''

    Function that actually trains and tests a single model.

    :param all_data: The whole data in a numpy array
    :type all_data: ndarray
    :param num_data_points: An array that lists the number of datapoints to use for training
    :type num_data_points: list
    :param source_tup: a tuple that represents the source data, aka (tree_num, num_rows, choice_set_size)
    :type source_tup: tuple
    :param save: to save or not to save. That is the question.
    :type save: boolean
    :return:
    :rtype:
    '''

    all_data = rp.data
    m_train = rp.m_train_all[-1]

    m, n = all_data.shape

    train_data = all_data[0:m_train, :]
    test_data = all_data[m_train:, :]

    out = []
    t_lls = []

    # Train at every number of data points specified.
    for num_points in rp.m_train_all:
        if num_points > m_train:
            continue

        # Grab training data
        data = train_data[0:num_points, :]

        # Build oracle
        oracle = Oracle(rp._replace(data=data))

        # Grab tree template and built NMNL2 object
        template = oracle.getTree_uncle()
        T_hat = trees.build_tree_from_template(template, n)
        nmnl = NMNL2(oracle, data=data)
        nmnl.constructTree(T_hat, n)

        # Optimize tree
        train_LL, ans = nmnl.optimize()

        # Test
        test_LL = nmnl.sumNLL_optim(data=test_data)
        out.append((train_LL, test_LL, nmnl.T, ans))
        t_lls.append(test_LL)

        log.info("m: {}, TrainLL: {}, TestLL: {}".format(num_points, train_LL, test_LL))

    return out, t_lls


'''

***************************************************************************
* All following functions are deprecated. They are the old code that relied
* on consts
***************************************************************************

'''


@DeprecationWarning
def main_read_grid():
    '''

    This function runs main_read several with each hyperparameter configuration specified in const. Also sets up
    the consts so main_read knows where the outpath is by setting consts.out_path.

    :param dir: The directory that contains all of the input data
    :type dir: string
    :param save: The
    :type save: boolean
    :return: None
    :rtype: None
    '''
    all_len = len(consts.grid_alpha) * len(consts.grid_uncle_alpha) * len(consts.grid_max_cut_value)
    id_range = ((consts.id - 1) * all_len // consts.num_ids, consts.id * all_len // consts.num_ids)

    i = 0
    for alpha, uncle_alpha, max_cut in itertools.product(consts.grid_alpha, consts.grid_uncle_alpha,
                                                         consts.grid_max_cut_value):

        if i >= id_range[0] and i < id_range[1]:
            # set the max cut value
            consts.alpha, consts.uncle_alpha, consts.max_cut_value = alpha, uncle_alpha, max_cut
            consts.out_path = consts.default_out_path + consts.dir_out_format.format(alpha, uncle_alpha, max_cut)

            log.info('\n')
            log.info("*** Testing with new hyper params*** alpha: {}, uncle alpha: {}, max cut: {}".format(alpha,
                                                                                                           uncle_alpha,
                                                                                                           max_cut))
            log.info("New output path: {}".format(consts.out_path))
            main_read()

        i += 1


def main_read():
    '''
    Should only be run through main_read_grid. Runs a single iteration of training and testing the model on all test
    files with a certain parameter setting.

    Does the following:

    loop through all files in data directory:
        extract information about the file
        call run_pre_data_test()

    :param data_dir: The data directory
    :type data_dir: string
    :param save: whether or not to save checkpoints /
    :type save:
    :return:
    :rtype:
    '''

    data_dir = consts.path
    save = consts.save
    log.info("Loading training files from %s", data_dir)
    if save: log.info("Writing out_2 to dir %s", consts.out_path)

    def parse(str):
        a = consts.filename_pattern.match(str)
        return a.group(1), a.group(2), a.group(3)

    all_out = []
    for filename in os.listdir(data_dir):

        try:
            data = np.loadtxt(data_dir + filename, delimiter=consts.delim)
            tree_num, _, choice_num = s_tup = parse(filename)

            out_path = get_out_path(s_tup)
            log.info("Tree number: %s, Num choice sets %s", tree_num, choice_num)

            # if we're saving stuff and the output file exists, then skip
            if save and os.path.isfile(out_path):
                log.info("Skipping because ouput exists...")
                continue

            out, t_lls = run_pre_data_test(data, consts.m_train_all, s_tup, save=save)
            all_out.append((tree_num, choice_num, t_lls))

        except Exception as e:
            log.error("Caught an exception! Skipping...")
            traceback.print_exc()

        dump_str(str(all_out), consts.out_path + consts.out_lls_pattern)


def run_pre_data_test(all_data, num_data_points, source_tup, save=False):
    '''

    Function that actually trains and tests a single model.

    :param all_data: The whole data in a numpy array
    :type all_data: ndarray
    :param num_data_points: An array that lists the number of datapoints to use for training
    :type num_data_points: list
    :param source_tup: a tuple that represents the source data, aka (tree_num, num_rows, choice_set_size)
    :type source_tup: tuple
    :param save: to save or not to save. That is the question.
    :type save: boolean
    :return:
    :rtype:
    '''
    m, n = all_data.shape

    train_data = all_data[0:consts.m_train, :]
    test_data = all_data[consts.m_train:m, :]

    out = []
    t_lls = []

    # Train at every number of data points specified.
    for num_points in num_data_points:
        if num_points > consts.m_train:
            continue

        # Grab training data
        data = train_data[0:num_points, :]

        # Build oracle
        oracle = Oracle(data)

        # Grab tree template and built NMNL2 object
        template = oracle.getTree_uncle()
        T_hat = trees.build_tree_from_template(template, n)
        nmnl = NMNL2(oracle, data=data)
        nmnl.constructTree(T_hat, n)

        # Optimize tree
        train_LL, ans = nmnl.optimize()

        # Test
        test_LL = nmnl.sumNLL_optim(data=test_data)
        out.append((train_LL, test_LL, nmnl.T, ans))
        t_lls.append(test_LL)

        log.info("m: {}, TrainLL: {}, TestLL: {}".format(num_points, train_LL, test_LL))

    if save:
        dump(out, consts.out_path + consts.out_pattern.format(*source_tup))

    return out, t_lls


def dump(obj, path):
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'wb') as f:
        pickle.dump(obj, f)


def dump_str(str, path):
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'w') as f:
        f.write(str)


def get_out_path(s_tup):
    return consts.out_path + consts.out_pattern.format(*s_tup)


'''

***************************************************************************
* All following functions are deprecated. They generate data on the fly.  *
***************************************************************************

'''


@DeprecationWarning
def main_generate():
    run_id = "only_benson"

    tree_list = trees.get_tree_list()

    log.info("Testing on %s trees...", len(tree_list))
    choice_set_sizes = [3]
    num_data_points = [20000, 30000, 40000, 50000]
    out_data = testAll(tree_list, choice_set_sizes, num_data_points)
    dump(out_data, consts.default_log_dir + "whole_run/" + str(run_id))

    create_plots(out_data, num_data_points)


@DeprecationWarning
def main():
    # Constants
    num_data_points = 1000000

    # Run
    tree_list = trees.get_tree_list()
    data_list = gen_oracle.get_oracles_from_trees(tree_list, num_points=num_data_points)
    oracle_list = [Oracle(dat) for dat in data_list]

    oracle_build_trees = [oracle.getTree_uncle() for oracle in oracle_list]

    print(oracle_build_trees)
    print([str(node) for node in tree_list])

    [trees.build_tree_from_template(template, tree_list[i].num_leaves) for i, template in
     enumerate(oracle_build_trees)]

    compare_list = [1 if trees.match_tree_to_template(tree_list[i], template) else 0 for i, template in
                    enumerate(oracle_build_trees)]

    print(compare_list)
    print(sum(compare_list))


@DeprecationWarning
def testAll(tree_list, choice_set_sizes, data_point_nums):
    return [[runSingleTest(tree, num_choices, data_point_nums) for num_choices in choice_set_sizes] for tree in
            tree_list]


@DeprecationWarning
def runSingleTest(sourceTree, num_choices, data_point_nums, save=False):
    log.debug("Starting single test {}".format(sourceTree.tag_name))

    out = []

    try:
        num_to_generate = data_point_nums[-1]
        all_data = gen_oracle.get_oracles_from_trees([sourceTree], num_points=num_to_generate, num_choices=num_choices)[
            0]

        truth_nmnl = NMNL2(None, None)
        truth_nmnl.T = sourceTree

        for num_points in data_point_nums:
            data = all_data[0:num_points, :]
            oracle = Oracle(data)
            template = oracle.getTree_uncle()
            T_hat = trees.build_tree_from_template(template, sourceTree.num_leaves)
            nmnl = NMNL2(oracle, data=data)
            nmnl.constructTree(T_hat, sourceTree.num_leaves)
            LL, ans = nmnl.optimize()
            truth_nmnl.setData(data)
            ground_LL = truth_nmnl.sumNLL_optim()
            out.append((LL, ground_LL, nmnl.T, sourceTree, ans))

        if save:
            dump(out, consts.default_log_dir + sourceTree.tag_name + "/" + str(num_choices))


    except Exception as e:  # change back
        log.error("Caught Exception {}".format(e))
    return out


@DeprecationWarning
def create_plots(data, x):
    import matplotlib.pyplot as plt

    for tree_data in data:
        for single_run in tree_data:
            seq_ll, seq_ground, _, _, _ = zip(*single_run);
            seq_ll = np.asarray(seq_ll)
            seq_ground = np.asarray(seq_ground)
            plt.plot(x, (seq_ll - seq_ground) / seq_ground, 'g^')
            plt.show()
