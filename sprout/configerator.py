import itertools
import os

import numpy as np
import yaml
from munch import munchify

yaml_path = "config/default.yaml"


def eval_yaml():
    with open(yaml_path, 'r') as stream:
        try:
            return munchify(yaml.load(stream))

        except yaml.YAMLError as exc:
            print(exc)
            raise


config = eval_yaml()


class FilesObj:
    """
    This class provides an interface to all output and input file paths along with some simple manipulations

    """

    def __init__(self, isScratch=True, raw_input_path=None):
        global config
        self.config = config
        out_conf = config.current.output
        sub_path = "scratch" if isScratch else "experiments"

        full_base_path = out_conf.output_format.format(scratch=sub_path, tag=config.current_tag)
        os.makedirs(full_base_path, exist_ok=True)

        def bp(s):
            os.makedirs(full_base_path + s, exist_ok=True)
            return full_base_path + s

        # Folders
        self.fig_path = bp("figures/")
        self.log_path = bp("logs/")
        self.raw_output_path = bp("raw_output/")
        self.checkpoint_path = bp("checkpoint/")

        # Files
        self.profile_path = "restats"
        self.final_data_path = self.raw_output_path + out_conf.final_out_data

        # Input path
        self.raw_data_path = full_base_path + "raw_data/" if raw_input_path is None else raw_input_path
        os.makedirs(self.raw_data_path, exist_ok=True)

        config.files = self


def grab_meta_iterator():
    """
    Yields a meta iterator as specified in the experiment spec to iterate over all parameters.
    :return:
    :rtype:
    """
    def grid_iterator():
        meta_params = config.current.meta_params
        grid_alpha = compute_array_range(**meta_params.alpha)
        grid_uncle_alpha = compute_array_range(**meta_params.uncle_alpha)
        grid_max_cut_value = compute_array_range(**meta_params.max_cut)
        all_len = len(grid_alpha) * len(grid_uncle_alpha) * len(grid_max_cut_value)
        id_range = ((config.runtime.id - 1) * all_len // config.runtime.num_ids,
                    config.runtime.id * all_len // config.runtime.num_ids)

        return itertools.islice(itertools.product(grid_alpha, grid_uncle_alpha, grid_max_cut_value), id_range[0],
                                id_range[1], 1)

    def single_iterator(alpha, uncle_alpha, max_cut_value):
        return iter([(alpha, uncle_alpha, max_cut_value)])

    return grid_iterator() if config.current.meta_run else single_iterator(**config.current.single_run_params)


def compute_array_range(start, stop, num):
    """
    the names of the variables line up with the yaml, this is critical
    Computes an array range from the given inputs

    :param start:
    :type start:
    :param stop:
    :type stop:
    :param num:
    :type num:
    :return:
    :rtype:
    """

    return np.arange(start, stop, (stop - start) / num)
