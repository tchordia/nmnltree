import logging
import os

import numpy as np
import pandas as pd

import sprout.consts as consts
import sprout.utils as utils
from sprout.nmnl.nmnl_impl import NMNL2
from sprout.test import nick_trees

log = logging.getLogger(__name__)


def dump_str(str, path):
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'w') as f:
        f.write(str)


# TODO: Clean up file paths and make sure this is still functional after the reorganization

def main():
    # Grab trees and construct nmnl objects
    all_trees = [nick_trees.build_tree_by_id(i) for i in range(1, 7)]
    all_nmnls = [NMNL2(None, None) for i in range(1, 7)]

    for i, tree in enumerate(all_trees):
        all_nmnls[i].T = tree

    data_dir = "datasets/"

    ground_out = []

    for filename in os.listdir(data_dir):
        print(filename)

        tree_num, _, choice_num = s_tup = utils.parse_filename(filename)
        tree_num, choice_num = int(tree_num), int(choice_num)

        data = np.loadtxt(data_dir + filename, delimiter=consts.delim)
        data = data[consts.m_train:, :]

        #
        if choice_num >= 3:

            print("On file {}".format(filename))
            print(data.max(axis=1).min())
            nmnl = all_nmnls[tree_num - 1]
            nmnl.setData(data)

            LL = nmnl.sumNLL_optim()
            ground_out.append((tree_num, choice_num, LL))
        else:
            print("Not 3")

    write_out_data(ground_out, "datasets/ground_lls.txt")
            # dump_str(str(ground_out), "datasets/ground_lls.txt")


def write_out_data(data_list, path):
    df_tree = pd.DataFrame(data_list)
    df_tree.columns = ['T', 'nc', 'll']
    df_tree.to_csv(path)


if __name__ == "__main__":
    main()
