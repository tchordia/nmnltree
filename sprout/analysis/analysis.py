import os

"""
****************************
* This file is deprecated  *
****************************
"""

######### TODO: CLEANUP THIS FILE
# TODO: make sure all output directories are from scratch folder

lls = None
import matplotlib.pyplot as plt
import pprint
import sprout.consts as consts
import numpy as np

pp = pprint.PrettyPrinter(indent=4)

optimal = .3, .05, .7

old = 0.01, 0.01, 0.65


def parse(str):
    a = consts.dir_out_pattern.match(str)
    return float(a.group(1)), float(a.group(2)), float(a.group(3))


def main():
    construct_df()

@DeprecationWarning
def plot():
    filename = "scratch/all_output/{}/ll_out.txt".format(consts.dir_out_format).format(*old)
    # filename = "out_old/{}/ll_out.txt".format(consts.dir_out_format).format(*optimal)

    lls = None

    with open(filename, "r") as f:
        lls = eval(f.read())

    with open("scratch/out_old/ground_ll.txt") as f:
        ground_lls = eval(f.read())

    map = {}
    for (t, n, l) in ground_lls:
        map[(t, n)] = l

    for tree_num in range(4, 7):
        for (t, n, l) in lls:
            t, n, l = int(t), int(n), [int(x) for x in l]
            if (t, n) in map:
                print(t, n)

                if t == tree_num:
                    ans = map[(t, n)]
                    ar = (np.asarray(l) - ans) / ans * 100

                    plt.plot(consts.m_train_all, ar, label="k = {}".format(n))
                    # plt.plot([map[(t, n)]] * 9)

                    print(l)
                    print(map[(t, n)])

        plt.legend()
        plt.title("Tree {}".format(tree_num))
        plt.xlabel('Num Data Points')
        plt.ylabel('% LL Test error')

        plt.savefig('fig/tree_{}'.format(tree_num))
        plt.show()

@DeprecationWarning
def construct_df():
    dir = "scratch/all_output"
    tuple_list = []
    tuple_list_last = []
    tuple_list_min = []

    map = {}
    avg = lambda l: sum(l) / float(len(l))

    total = 0
    for subdir in os.listdir(dir):
        with open("{}/{}/ll_out.txt".format(dir, subdir), "r") as f:
            lls = eval(f.read())
            ah, uah, mch = parse(subdir)

            if lls is not None and len(lls) > 0:
                total += 1
                # tuple_list = tuple_list + [(i, int(t), int(n), float(c), float(ah), float(uah), float(mch)) for t, n, l in lls for i, c in enumerate(l)]
                # tuple_list_min = tuple_list + [(int(t), int(n), min(map(float, l)), float(ah), float(uah), float(mch)) for t, n, l in lls]
                # tuple_list_last = tuple_list + [(int(t), int(n), float(l[-1]), float(ah), float(uah), float(mch)) for t, n, l in lls]
                print([l[-1] for t, n, l in lls])
                map[subdir] = avg([l[-1] for t, n, l in lls])

    minkey = min(map, key=map.get)
    print(minkey, total)
    pp.pprint(map)

    return

    # df = pd.DataFrame(tuple_list)
    # df_last = pd.DataFrame(tuple_list_last)
    #
    # df.columns = ['index', 'T', 'nc', 'll', 'alpha', 'uncle_alpha', 'mincut']
    # df_last.columns = ['index', 'T', 'nc', 'll', 'alpha', 'uncle_alpha', 'mincut']
    #
    # df.to_csv("all_output/all_data.csv")
    #
    # return df, df_last


if __name__ == "__main__":
    plot()



    #
    # try:
    #     data = np.loadtxt(dir + filename, delimiter=consts.delim)
    #     tree_num, _, choice_num = s_tup = parse(filename)
    #
    #     out_path = get_out_path(s_tup)
    #     log.info("Tree number: %s, Num choice sets %s", tree_num, choice_num)
    #
    #     # if we're saving stuff and the output file exists, then skip
    #     if save and os.path.isfile(out_path):
    #         log.info("Skipping because ouput exists...")
    #         continue
    #
    #     out_2, t_lls = run_pre_data_test(data, consts.m_train_all, s_tup, save=save)
    #     all_out.append((tree_num, choice_num, t_lls))
    #
    # except Exception as e:
    #     log.error("Caught an exception! Skipping...")
    #     traceback.print_exc()

# with open("scratch/out_2/ll_out_all.txt", "r") as f:
#     lls = eval(f.read())
#
# with open("out_2/ground_ll.txt", "r") as f:
#     ground_lls = eval(f.read())
#
#
# if lls is not None:
#     tuple_list = [ (i, int(a), int(b), float(c)) for a, b, l in lls for i, c in enumerate(l) ]
#     df = pd.DataFrame(tuple_list)
#     df.columns = ['index', 'T', 'nc', 'll']
#
#     df_3 = df[df['T'] == 3]
#     print(df_3)
#
#
#     df_3.groupby('nc')['ll'].plot(x = df_3.index, legend = True)
#
#     plt.show()
