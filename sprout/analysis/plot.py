import pandas as pd
import matplotlib.pyplot as plt
import os


def plot(df, T, alpha, ua, mc):

    fig = plt.figure()
    ax = fig.gca()

    plot_data = df.query('T == @T & alpha == @alpha & uncle_alpha == @ua & max_cut == @mc')
    plot_data.groupby("nc").plot(x="m", y="ll", ax = ax)

def plot_test():
    data_dir = "/Users/Tanmay/PycharmProjects/CIS700/scratch/experiment_default/raw_output"

    all_data_list = [pd.read_csv(filename) for filename in os.listdir(data_dir)]
    all_data = pd.concat(all_data_list)

    plot()

if __name__ == "__main__":
    plot_test()

